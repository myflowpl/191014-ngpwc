import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }
  navigateToMusic() {
    return browser.get(browser.baseUrl + '/music/artists') as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root app-header div b')).getText() as Promise<string>;
  }
  getFirstArtistText() {
    return element(by.css('ul li > a')).getText() as Promise<string>;
  }
}
