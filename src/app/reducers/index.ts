import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCounter from '../counter/counter.reducer';
import * as fromMusic from '../music/music.reducer';


export interface State {

  [fromCounter.counterFeatureKey]: fromCounter.State;
  [fromMusic.musicFeatureKey]: fromMusic.State;
}

export const reducers: ActionReducerMap<State> = {

  [fromCounter.counterFeatureKey]: fromCounter.reducer,
  [fromMusic.musicFeatureKey]: fromMusic.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
