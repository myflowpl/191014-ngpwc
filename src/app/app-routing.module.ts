import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared/components/home/home.component';
import { GuestbookComponent } from './shared/components/guestbook/guestbook.component';
import { AboutComponent } from './shared/components/about/about.component';
import { CounterComponent } from './counter/counter/counter.component';


const routes: Routes = [{
  path: '',
  component: HomeComponent,
  children: [{
    path: 'guestbook',
    component: GuestbookComponent
  }, {
    path: 'about',
    component: AboutComponent
  }]
}, {
  path: 'music',
  loadChildren: () => import('./music/music.module').then(m => m.MusicModule)
}, {
  path: 'counter',
  component: CounterComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
