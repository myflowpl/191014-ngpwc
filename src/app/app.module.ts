import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { API_BASE_PATH, IMAGES_BASE_PATH } from './tokens';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { CounterModule } from './counter/counter.module';
import { EffectsModule } from '@ngrx/effects';
import { CounterEffects } from './counter/counter.effects';
import { MusicEffects } from './music/music.effects';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CounterModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([CounterEffects, MusicEffects])
  ],
  providers: [
    {
      provide: API_BASE_PATH,
      useValue: 'http://localhost:3000'
    },
    {
      provide: IMAGES_BASE_PATH,
      useValue: 'http://www.songnotes.cc/images/artists/'
    }
    // , {
    //   provide: APP_INITIALIZER,
    //   useFactory: (config) => {
    //     // todo load config from server
    //     config.setValues(configFromServer);
    //   },
    //   deps: [ConfigService]
    // },
    // ConfigService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
