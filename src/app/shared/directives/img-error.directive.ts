import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appImgError]',
  host: {
    '(error)': 'onError()',
    '[src]': 'src'
  }
})
export class ImgErrorDirective {

  @Input()
  src: string;

  onError() {
    this.src = '/assets/no-img.png';
  }

}
