import { Directive, ElementRef, Renderer2, HostBinding, HostListener, Renderer } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {


  @HostBinding('style.text-decoration')
  decoration = 'underline';

  constructor(
    private el: ElementRef,
    private renderer: Renderer
  ) {
    console.log('directive', this.el);
  }

  @HostListener('mouseover')
  onmouseover() {
    this.decoration = 'underline';
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', 'yellow');
  }
  @HostListener('mouseout')
  onmouseout() {
    this.decoration = '';
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', '');
  }

}
