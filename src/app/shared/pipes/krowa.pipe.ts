import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'krowa'
})
export class KrowaPipe implements PipeTransform {

  transform(value: string, word: string = 'krowa'): string {

    if (typeof value !== 'string') { return value; }

    const words = value.split(' ');
    if (words.length > 2) {
      const index = Math.ceil(Math.random() * words.length - 1);
      words.splice(index, 0, word);
    }
    return words.join(' ');
  }

}
