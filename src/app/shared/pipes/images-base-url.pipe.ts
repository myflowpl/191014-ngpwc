import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IMAGES_BASE_PATH } from '../../tokens';

@Pipe({
  name: 'imagesBaseUrl'
})
export class ImagesBaseUrlPipe implements PipeTransform {

  constructor(
    @Inject(IMAGES_BASE_PATH) public imgBase: string
  ) {}
  transform(value: any, ...args: any[]): any {
    return this.imgBase + value;
  }

}
