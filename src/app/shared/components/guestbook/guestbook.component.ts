import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface Item {
  id: number;
  name: string;
}

@Component({
  selector: 'app-guestbook',
  templateUrl: './guestbook.component.html',
  styleUrls: ['./guestbook.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class GuestbookComponent implements OnInit, OnDestroy {

  @ViewChild('myDiv', {static: true})
  myDiv: ElementRef;

  title: string;
  amount = 55;
  step = 10;
  visible = false;
  items$: Observable<Item[]>;
  items: Item[];
  sub;
  currentDate = Date.now();

  destroy$ = new Subject();

  constructor(private http: HttpClient) {   }

  ngOnInit() {
    this.title = 'Title z komponentu';
    this.items$ = this.http.get<Item[]>('/assets/items.json').pipe(
      takeUntil(this.destroy$)
    );
    // this.sub = this.items$.subscribe({
    //   next: (v) => this.items = v
    // });
    // subscription.unsubscribe();
  }

  onToggleClick() {
    console.log('DIV', this.myDiv);
    this.visible = !this.visible;
  }

  ngOnDestroy() {
    this.destroy$.next();
    // if(this.sub) {
    //   this.sub.unsubscribe();
    // }
  }
}
