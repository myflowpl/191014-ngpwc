import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { GuestbookComponent } from './components/guestbook/guestbook.component';
import { AboutComponent } from './components/about/about.component';
import { RouterModule } from '@angular/router';
import { KrowaPipe } from './pipes/krowa.pipe';
import { HighlightDirective } from './directives/highlight.directive';
import { UnlessDirective } from './directives/unless.directive';
import { ImagesBaseUrlPipe } from './pipes/images-base-url.pipe';
import { ImgErrorDirective } from './directives/img-error.directive';
import {MatButtonModule} from '@angular/material/button';
import { UserModule } from '../user/user.module';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    GuestbookComponent,
    AboutComponent,
    KrowaPipe,
    HighlightDirective,
    UnlessDirective,
    ImagesBaseUrlPipe,
    ImgErrorDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    UserModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ImagesBaseUrlPipe,
    ImgErrorDirective,
    UserModule
  ]
})
export class SharedModule { }
