import { Injectable, Inject } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CounterActions, CounterActionTypes, AmountSavedCounters, SavingCounters } from './counter.actions';
import { filter, switchMap, take, catchError, map, delay, startWith } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { HttpClient } from '@angular/common/http';
import { API_BASE_PATH } from '../tokens';

@Injectable()
export class CounterEffects {

  @Effect()
  save$ = this.actions$.pipe(
    // filter(action => action.type === CounterActionTypes.Up)
    ofType(CounterActionTypes.Up, CounterActionTypes.Down),
    switchMap(action => this.store.select('counter', 'amount').pipe(take(1))),
    switchMap(amount => {
      return this.http.post(this.apiBase + '/amounts', {amount}).pipe(
        catchError(err => EMPTY),
        map(res => new AmountSavedCounters({res})),
        delay(2000),
        startWith(new SavingCounters())
      );
    })
  );

  constructor(
    private actions$: Actions<CounterActions>,
    private store: Store<State>,
    private http: HttpClient,
    @Inject(API_BASE_PATH) private apiBase: string
  ) { }

}
