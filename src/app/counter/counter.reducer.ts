import { Action } from '@ngrx/store';
import { CounterActionTypes, CounterActions } from './counter.actions';

export interface Item {
  name: string;
  amount: number;
}

export const counterFeatureKey = 'counter';

export interface State {
  amount: number;
  step: number;
  status: 'saving' | 'synced';
  items: Item[];
}

export const initialState: State = {
  amount: 5,
  step: 1,
  status: 'synced',
  items: [
    {name: 'test item 1', amount: 1},
    {name: 'test item 2', amount: 2},
    {name: 'test item 3', amount: 3},
    {name: 'test item 4', amount: 4},
    {name: 'test item 5', amount: 5},
    {name: 'test item 6', amount: 4},
    {name: 'test item 7', amount: 6},
    {name: 'test item 8', amount: 9},
    {name: 'test item 8', amount: 4},
    {name: 'test item 9', amount: 1},
    {name: 'test item 10', amount: 9}
  ]
};

export function reducer(state = initialState, action: CounterActions): State {
  switch (action.type) {
    case (CounterActionTypes.Up): {
      return {
        ...state,
        amount: state.amount + state.step
      };
    }
    case (CounterActionTypes.Down): {
      return {
        ...state,
        amount: state.amount - state.step
      };
    }
    case (CounterActionTypes.Step): {
      return {
        ...state,
        step: action.payload.step
      };
    }
    case (CounterActionTypes.Saving): {
      return {
        ...state,
        status: 'saving'
      };
    }
    case (CounterActionTypes.AmountSaved): {
      return {
        ...state,
        status: 'synced'
      };
    }
    default:
      return state;
  }
}
