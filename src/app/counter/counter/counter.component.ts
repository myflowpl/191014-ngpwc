import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../../reducers';
import { UpCounters, DownCounters, StepCounters } from '../counter.actions';
import { selectCounterAmount, selectCounterStep, selectCounterStatus, selectCounterItems, selectCounterItemsFiltered } from '../counter.selectors';
import { Item } from '../counter.reducer';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  amount$: Observable<number>;
  step$: Observable<number>;
  status$: Observable<string>;
  items$: Observable<Item[]>;

  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.amount$ = this.store.select(selectCounterAmount);
    this.step$ = this.store.select(selectCounterStep);
    this.status$ = this.store.select(selectCounterStatus);
    this.items$ = this.store.select(selectCounterItemsFiltered);
  }
  onUpClick() {
    this.store.dispatch(new UpCounters());
  }
  onDownClick() {
    this.store.dispatch(new DownCounters());
  }
  onStepChange(e) {
    this.store.dispatch(new StepCounters({
      step: parseInt(e.target.value, 10)
    }));
  }

}
