import { Action } from '@ngrx/store';

export interface StepActionPayload {
  step: number;
}

export enum CounterActionTypes {
  Up = '[Counter] Up',
  Down = '[Counter] Down',
  Step = '[Counter] Step',
  Saving = '[Counter] Saving',
  AmountSaved = '[Counter] Amount Saved',
}

export class UpCounters implements Action {
  readonly type = CounterActionTypes.Up;
}
export class DownCounters implements Action {
  readonly type = CounterActionTypes.Down;
}
export class StepCounters implements Action {
  readonly type = CounterActionTypes.Step;
  constructor(public payload: {step: number}) {}
}
export class AmountSavedCounters implements Action {
  readonly type = CounterActionTypes.AmountSaved;
  constructor(public payload: {res: any}) {}
}
export class SavingCounters implements Action {
  readonly type = CounterActionTypes.Saving;
}

export type CounterActions = UpCounters | DownCounters | StepCounters | AmountSavedCounters | SavingCounters;
