import { State } from '../reducers';
import { createSelector } from '@ngrx/store';

export const selectCounter = (state: State) => state.counter;

export const selectCounterAmount = createSelector(
  selectCounter,
  (counter) => counter.amount
);

export const selectCounterStep = createSelector(
  selectCounter,
  (counter) => counter.step
);

export const selectCounterStatus = createSelector(
  selectCounter,
  (counter) => counter.status
);

export const selectCounterItems = createSelector(
  selectCounter,
  (counter) => counter.items
);

export const selectCounterItemsFiltered = createSelector(
  selectCounterAmount,
  selectCounterItems,
  (amount, items) => items.filter(i => i.amount >= amount)
);
