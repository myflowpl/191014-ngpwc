import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Artist } from '../../models';
import { API_BASE_PATH } from '../../tokens';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(
    private http: HttpClient,
    @Inject(API_BASE_PATH) private apiBasePath: string
  ) { }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.apiBasePath + '/artists');
  }

  getArtistById(id: number): Observable<Artist> {
    return this.http.get<Artist>(this.apiBasePath + '/artists/' + id);
  }

  updateArtist(data: Artist): Observable<Artist> {
    return this.http.patch<Artist>(this.apiBasePath + '/artists/' + data.id, data);
  }

  deleteArtist(id: number): Observable<void> {
    return this.http.delete<void>(this.apiBasePath + '/artists/' + id);
  }
}
