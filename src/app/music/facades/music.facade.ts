import { Injectable } from '@angular/core';
import { MusicService } from '../services/music.service';
import { Observable, BehaviorSubject, EMPTY } from 'rxjs';
import { Artist } from '../../models';
import { tap, filter, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { State } from '../../reducers';
import { LoadArtists, UpdateArtist, DeleteArtistSuccess } from '../music.actions';

@Injectable({ providedIn: 'root' })
export class MusicFacade {

  // artists$ = new BehaviorSubject<Artist[]>([]);
  artists$ = this.store.select('music', 'artists');

  constructor(
    private store: Store<State>,
    private musicService: MusicService
  ) { }

  artistById$(id: number): Observable<Artist> {
    return this.artists$.pipe(
      map(artists => artists.find(artist => artist.id === id))
    );
  }

  loadArtists() {
    this.store.dispatch(new LoadArtists());
  }

  updateArtist(artist: Artist) {
    this.store.dispatch(new UpdateArtist({ artist }));
  }

  deleteArtist(id: any) {
    // uproszczenie, ale nie jest to dobre rozwiązanie
    return this.musicService.deleteArtist(id).pipe(
      tap(() => this.store.dispatch(new DeleteArtistSuccess({ id: parseInt(id, 10) })))
    );
  }
}
