import { Component, OnInit } from '@angular/core';
import { Artist } from '../../../models';
import { MusicService } from '../../services/music.service';
import { MusicFacade } from '../../facades/music.facade';

@Component({
  selector: 'app-artists-page',
  templateUrl: './artists-page.component.html',
  styleUrls: ['./artists-page.component.css']
})
export class ArtistsPageComponent implements OnInit {

  public artists$ = this.musicFacade.artists$;

  constructor(
    public musicFacade: MusicFacade
  ) { }

  ngOnInit() {
    this.musicFacade.loadArtists();
  }

}
