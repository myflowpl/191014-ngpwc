import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistViewPageComponent } from './artist-view-page.component';

describe('ArtistViewPageComponent', () => {
  let component: ArtistViewPageComponent;
  let fixture: ComponentFixture<ArtistViewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistViewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistViewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
