import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { Artist } from '../../../models';
import { MusicService } from '../../services/music.service';
import { MusicFacade } from '../../facades/music.facade';

@Component({
  selector: 'app-artist-view-page',
  templateUrl: './artist-view-page.component.html',
  styleUrls: ['./artist-view-page.component.css']
})
export class ArtistViewPageComponent implements OnInit {

  id$: Observable<number>;
  artist$: Observable<Artist>;
  isEdit = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private musicFacade: MusicFacade
  ) { }

  ngOnInit() {
    this.id$ = this.route.params.pipe(
      map(params => parseInt(params.id, 10))
    );

    this.artist$ = this.id$.pipe(
      switchMap(id =>
        this.musicFacade.artistById$(id)
      )
    );
  }

  onArtistSave(artist: Artist) {
    console.log('SAVE THE DATA', artist);
    this.musicFacade.updateArtist(artist);
  }
  onDelete(id) {
    this.musicFacade.deleteArtist(id).subscribe(() => {
      this.router.navigate(['music', 'artists']);
    });
  }
}
