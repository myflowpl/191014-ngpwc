import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtistsPageComponent } from './pages/artists-page/artists-page.component';
import { ArtistViewPageComponent } from './pages/artist-view-page/artist-view-page.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'artists',
  pathMatch: 'full'
}, {
  path: 'artists',
  component: ArtistsPageComponent,
  children: [{
    path: 'view/:id',
    component: ArtistViewPageComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
