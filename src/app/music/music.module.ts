import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MusicRoutingModule } from './music-routing.module';
import { ArtistsPageComponent } from './pages/artists-page/artists-page.component';
import { ArtistViewPageComponent } from './pages/artist-view-page/artist-view-page.component';
import { ArtistComponent } from './components/artist/artist.component';
import { ArtistFormComponent } from './components/artist-form/artist-form.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [ArtistsPageComponent, ArtistViewPageComponent, ArtistComponent, ArtistFormComponent],
  imports: [
    CommonModule,
    MusicRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class MusicModule { }
