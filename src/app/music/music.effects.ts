import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { MusicActions, MusicActionTypes, LoadArtistsSuccess, LoadArtistsError, UpdateArtistSuccess, UpdateArtistError, LoadArtists } from './music.actions';
import { MusicService } from './services/music.service';
import { switchMap, map, catchError, takeUntil } from 'rxjs/operators';
import { Artist } from '../models';
import { of, from } from 'rxjs';



@Injectable()
export class MusicEffects {

  @Effect()
  loadArtists$ = this.actions$.pipe(
    ofType(MusicActionTypes.LoadArtists),
    switchMap(() => {
      return this.musicService.getArtists().pipe(
        map((artists: Artist[]) => new LoadArtistsSuccess({ artists })),
        catchError(err => of(new LoadArtistsError({ err })))
      );
    })
  );

  @Effect()
  updateArtists$ = this.actions$.pipe(
    ofType(MusicActionTypes.UpdateArtist),
    // takeUntil(this.actions$.pipe(ofType(MusicActionTypes.UpdateArtistCancel))),
    switchMap((action) => {
      return this.musicService.updateArtist(action.payload.artist).pipe(
        switchMap((artist: Artist) => from([
          new UpdateArtistSuccess({ artist }),
          new LoadArtists()
        ])),
        catchError(err => of(new UpdateArtistError({ err })))
      );
    })
  );

  constructor(
    private actions$: Actions<MusicActions>,
    private musicService: MusicService
  ) { }

}
