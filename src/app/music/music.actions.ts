import { Action } from '@ngrx/store';
import { Artist } from '../models';

export enum MusicActionTypes {
  LoadArtists = '[Music] Load Artists',
  LoadArtistsSuccess = '[Music] Load Artists Success',
  LoadArtistsError = '[Music] Load Artists Error',

  UpdateArtist = '[Music] Update Artist',
  UpdateArtistSuccess = '[Music] Update Artist Success',
  UpdateArtistError = '[Music] Update Artist Error',

  DeleteArtistSuccess = '[Music] Delete Artist Success',
}

export class LoadArtists implements Action {
  readonly type = MusicActionTypes.LoadArtists;
}
export class LoadArtistsSuccess implements Action {
  readonly type = MusicActionTypes.LoadArtistsSuccess;
  constructor(public payload: { artists: Artist[] }) { }
}
export class LoadArtistsError implements Action {
  readonly type = MusicActionTypes.LoadArtistsError;
  constructor(public payload: { err: any }) { }
}
// update artist
export class UpdateArtist implements Action {
  readonly type = MusicActionTypes.UpdateArtist;
  constructor(public payload: { artist: Artist }) { }
}
export class UpdateArtistSuccess implements Action {
  readonly type = MusicActionTypes.UpdateArtistSuccess;
  constructor(public payload: { artist: Artist }) { }
}
export class UpdateArtistError implements Action {
  readonly type = MusicActionTypes.UpdateArtistError;
  constructor(public payload: { err: any }) { }
}

export class DeleteArtistSuccess implements Action {
  readonly type = MusicActionTypes.DeleteArtistSuccess;
  constructor(public payload: { id: number }) { }
}

export type MusicActions =
  LoadArtists |
  LoadArtistsSuccess |
  UpdateArtist |
  UpdateArtistSuccess |
  DeleteArtistSuccess |
  UpdateArtistError |
  LoadArtistsError;
