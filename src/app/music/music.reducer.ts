import { Action } from '@ngrx/store';
import { Artist } from '../models';
import { MusicActionTypes, MusicActions } from './music.actions';


export const musicFeatureKey = 'music';

export interface State {
  artists: Artist[];
}

export const initialState: State = {
  artists: [],
};

export function reducer(state = initialState, action: MusicActions): State {
  switch (action.type) {
    case (MusicActionTypes.LoadArtistsSuccess): {
      return {
        ...state,
        artists: action.payload.artists
      };
    }
    case (MusicActionTypes.UpdateArtistSuccess): {
      return {
        ...state,
        artists: state.artists.map(artist => {
          if (artist.id === action.payload.artist.id) {
            return action.payload.artist;
          }
          return artist;
        })
      };
    }
    case (MusicActionTypes.DeleteArtistSuccess): {
      return {
        ...state,
        artists: state.artists.filter(artist => artist.id !== action.payload.id)
      };
    }
    default:
      return state;
  }
}
