import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Artist } from '../../../models';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-artist-form',
  templateUrl: './artist-form.component.html',
  styleUrls: ['./artist-form.component.css']
})
export class ArtistFormComponent implements OnInit {

  _artist: Artist;

  @Input()
  set artist(artist: Artist) {
    this._artist = artist;
    if (artist) {
      this.artistForm.patchValue(artist);
    }
  }

  @Output()
  save = new EventEmitter<Artist>();

  artistForm = this.fb.group({
    id: ['', Validators.required],
    name: ['', Validators.required],
    img: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }
  onSubmit() {
    if (this.artistForm.valid) {
      this.save.next(this.artistForm.value);
    }
  }

}
