import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, SimpleChanges, Inject } from '@angular/core';
import { Artist } from '../../../models';
import { IMAGES_BASE_PATH } from '../../../tokens';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit, OnChanges {

  @Input()
  artist: Artist;

  constructor() { }

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges) {
    // console.log('changes', changes);
  }
}
