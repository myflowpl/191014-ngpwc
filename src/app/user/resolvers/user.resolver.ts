import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { User } from '../../models';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {
  constructor(
    private userService: UserService
  ) {}
  resolve(): Observable<User> {
    return this.userService.user$.pipe(
      take(1)
    );
  }
}
