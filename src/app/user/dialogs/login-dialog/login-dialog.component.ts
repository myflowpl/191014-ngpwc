import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {

  loginForm = this.fb.group({
    username: ['Admin', [Validators.required], [this.validateUserName.bind(this)]],
    password: ['admin', [Validators.required]],
  });
  errorMessage: string;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    public dialog: MatDialogRef<LoginDialogComponent>
  ) { }

  validateUserName(control: AbstractControl) {
    return this.authService.doUserNameExists(control.value);
  }
  ngOnInit() {

  }
  onSubmit() {
    this.errorMessage = '';
    const {username, password} = this.loginForm.value;
    this.authService.login(username, password).subscribe(
      user => this.dialog.close(user),
      err => this.errorMessage = err.message
    );
  }

}
