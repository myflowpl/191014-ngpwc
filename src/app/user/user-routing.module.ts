import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfilePageComponent } from './pages/user-profile-page/user-profile-page.component';
import { AuthGuard } from './guards/auth.guard';
import { UserResolverService } from './resolvers/user.resolver';
import { SessionGuardDirective } from './directives/session-guard.directive';


const routes: Routes = [{
  path: 'profile',
  component: UserProfilePageComponent,
  canActivate: [AuthGuard],
  // watch: [SessionGuardDirective],
  data: {
    sessionGuard: true,
    user: null,
  },
  resolve: {
    user: UserResolverService
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
