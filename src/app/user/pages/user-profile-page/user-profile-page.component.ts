import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../../models';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-profile-page',
  templateUrl: './user-profile-page.component.html',
  styleUrls: ['./user-profile-page.component.css']
})
export class UserProfilePageComponent implements OnInit, OnDestroy {

  user: User;
  subs = new Subscription();
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {

    this.user = this.route.snapshot.data.user;


    // const sub = this.userService.user$.subscribe(user => {
    //   if (!user) {
    //     console.log('LOGOUT');
    //     this.router.navigateByUrl('/');
    //   }
    // });
    // this.subs.add(sub);

  }
  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
