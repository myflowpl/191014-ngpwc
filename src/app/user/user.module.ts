import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginDialogComponent } from './dialogs/login-dialog/login-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { UserProfilePageComponent } from './pages/user-profile-page/user-profile-page.component';
import { HasRoleDirective } from './directives/has-role.directive';
import { SessionGuardDirective } from './directives/session-guard.directive';

@NgModule({
  declarations: [LoginDialogComponent, UserProfilePageComponent, HasRoleDirective, SessionGuardDirective],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule
  ],
  entryComponents: [LoginDialogComponent],
  exports: [
    HasRoleDirective,
    SessionGuardDirective
  ]
})
export class UserModule { }
