import { Directive, Input, TemplateRef, ViewContainerRef, OnDestroy } from '@angular/core';
import { UserService } from '../services/user.service';
import { Subject, BehaviorSubject, Subscription, combineLatest } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnDestroy {

  sub: Subscription;
  user$ = this.userService.user$;
  role$ = new BehaviorSubject(null);

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private userService: UserService
  ) {
    this.sub = combineLatest(this.user$, this.role$)
      .pipe(
        map(([user, role]) => user && user.role === role),
        distinctUntilChanged()
      )
      .subscribe((condition) => {
        if (condition) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
      });
  }

  @Input()
  set appHasRole(role) {
    this.role$.next(role);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
