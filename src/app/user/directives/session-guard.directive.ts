import { Directive, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appSessionGuard]'
})
export class SessionGuardDirective implements OnInit, OnDestroy {

  sub: Subscription;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.sub = this.userService.user$.subscribe(user => {
      if (!user) {
        this.router.navigateByUrl('/');
        this.sub.unsubscribe();
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
