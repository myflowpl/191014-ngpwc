import { Injectable, Inject } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { User } from '../../models';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../dialogs/login-dialog/login-dialog.component';
import { HttpClient } from '@angular/common/http';
import { API_BASE_PATH } from '../../tokens';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    @Inject(API_BASE_PATH) private basePath: string
  ) { }

  login(username, password): Observable<User> {
    // MOCK LOGOWANIA !!!
    return this.http.get<User[]>(this.basePath + `/users`, {
      params: {
        name: username,
        password
      }
    }).pipe(
      map(users => users[0]),
      switchMap(user => {
        if (user) {
          return of(user);
        }
        return throwError({ message: 'User Not Found' });
      })
    );
  }

  doUserNameExists(value) {
    return of(null);
    return of({ usernameTaken: true });
  }
}
