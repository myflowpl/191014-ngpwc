import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { User } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class UserStorageService {

  key = 'user-session';

  getUser(): Observable<User | null> {
    const userStr = localStorage.getItem(this.key);
    if (userStr) {
      return of(JSON.parse(userStr));
    }
    return EMPTY;
  }
  setUser(user: User | null): Observable<void> {
    if (user) {
      const userStr = JSON.stringify(user);
      localStorage.setItem(this.key, userStr);
    } else {
      localStorage.removeItem(this.key);
    }
    return EMPTY;
  }
}
