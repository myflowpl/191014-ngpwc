import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, BehaviorSubject, EMPTY, of, throwError } from 'rxjs';
import { User } from '../../models';
import { LoginDialogComponent } from '../dialogs/login-dialog/login-dialog.component';
import { tap, switchMap } from 'rxjs/operators';
import { UserStorageService } from './user-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user$ = new BehaviorSubject<User | null>(null);

  get user$() {
    return this._user$.asObservable();
  }

  constructor(
    private dialog: MatDialog,
    private storage: UserStorageService
  ) {
    this.storage.getUser().subscribe(user => {
      this._user$.next(user);
    });
    this._user$.pipe(
      switchMap(user => this.storage.setUser(user))
    ).subscribe();
  }

  loginDialog(): Observable<User> {
    const dialogRef = this.dialog.open(LoginDialogComponent, {});
    return dialogRef.afterClosed().pipe(
      switchMap(user => {
        if (user) {
          return of(user);
        }
        return throwError({ message: 'Login canceled' });
      }),
      tap(user => this._user$.next(user))
    );
  }

  logout(): Observable<void> {
    this._user$.next(null);
    return EMPTY; // TODO auth service do zabicia sesji po stronie serwera
  }
}
