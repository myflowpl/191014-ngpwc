import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { UserStorageService } from './user-storage.service';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: UserStorageService,
      useValue: {

      }
    }]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();

    expect(service.logout()).toBeTruthy();
  });
});
