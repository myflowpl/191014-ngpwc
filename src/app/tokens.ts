import { InjectionToken } from '@angular/core';

export const API_BASE_PATH = new InjectionToken('API BASE PATH');

export const IMAGES_BASE_PATH = new InjectionToken('IMAGES BASE PATH');
