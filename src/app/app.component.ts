import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './user/services/user.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngpwc';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    // combineLatest(
    //   this.userService.user$,
    //   this.route.children[0].data
    // ).subscribe(([user, data]) => {
    //   console.log(user, data);
    //   if (!user && data.sessionGuard) {
    //     this.router.navigateByUrl('/');
    //   }
    // });
  }
}
